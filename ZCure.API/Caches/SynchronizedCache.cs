﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Caches
{
    public abstract class SynchronizedCache<TKey, TItem> : Cache<TKey, TItem> where TKey : notnull
    {
        public abstract TimeSpan UpdateInterval { get; }

        protected Timer _timer;

        public SynchronizedCache()
        {
            _timer = new Timer((x) => Sync(), null, TimeSpan.Zero, UpdateInterval);
        }

        ~SynchronizedCache()
        {
            _timer.Dispose();
        }

        public abstract void Sync();
    }
}
