﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Caches
{
    public interface ICache
    {
    }

    public interface ICache<TKey, TItem> : ICache
    {
        public void Add(TKey key, TItem item);
        public bool Contains(TKey key);
        public TItem? Get(TKey key);
        public void Update(TKey key, TItem item);

        public IEnumerable<TItem> GetAll();
        public IEnumerable<TItem> Where(Predicate<TItem> predicate);
        public IEnumerable<TItem> Where(Func<TKey, TItem, bool> predicate);
    }
}
