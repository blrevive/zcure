﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Database;
using ZCure.API.Models.Store;
using ZCure.API.Serializers;

namespace ZCure.API.Caches
{
    public class StoreCache : SynchronizedCache<string, string>
    {
        public override TimeSpan UpdateInterval => TimeSpan.FromHours(1);

        private Repositories _database;

        public StoreCache(Repositories database)
            : base()
        {
            _database = database;
        }

        public string GetCachedStore()
            => Get("store")!;

        public string GetCachedOffers()
            => Get("offers")!;


        public override void Sync()
        {
            _log.Debug("updating store cache");
            UpdateStore();
            UpdateOffers();
            _log.Information("updated store cache");
        }

        protected void UpdateStore()
        {
            _log.Debug("updating cached store info");
            ZCStoreInfo storeInfo = new ZCStoreInfo();
            storeInfo.StoreItems = _database.Store.GetList().ToArray();

            string serializedStoreInfo = ZCXmlSerializer.Serialize(storeInfo);
            
            lock(_itemCache)
            {
                _itemCache["store"] = serializedStoreInfo;
            }
            _log.Information("updated cached store info");
        }

        protected void UpdateOffers()
        {
            _log.Debug("updating cached offers info");
            StoreOfferInfo offerInfo = new StoreOfferInfo();
            offerInfo.OfferItems = _database.Offers.GetList().ToList();

            string serializedOfferInfo = ZCXmlSerializer.Serialize(offerInfo);

            lock(_itemCache)
            {
                _itemCache["offers"] = serializedOfferInfo;
            }
            _log.Information("updated cached offers info");
        }
    }
}
