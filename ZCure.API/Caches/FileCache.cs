﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Caches
{
    public class FileCache : Cache<string, byte[]>
    {
        private ZCureApp _app;

        public FileCache(ZCureApp app)
        {
            _app = app;
        }

        public void LoadFromDisk()
        {
            if (!Directory.Exists(_app.Configuration.FileCacheDirectory))
            {
                _log.Debug($"file cache directory {_app.Configuration.FileCacheDirectory} does not exist");
                return;
            }

            foreach(var file in Directory.GetFiles(_app.Configuration.FileCacheDirectory))
                Add(Path.GetFileName(file), File.ReadAllBytes(file));

            _log.Information("cached {Count} files from disk", _itemCache.Count);
        }

        public string? GetString(string fileName, Encoding encoding)
        {
            byte[]? content = Get(fileName);
            
            if (content == null)
                return null;

            return encoding.GetString(content);
        }

        public string? GetString(string fileName)
            => GetString(fileName, Encoding.UTF8);

        public void Add(string fileName, string content, Encoding encoding)
            => Add(fileName, encoding.GetBytes(content));

        public void Add(string fileName, string content)
            => Add(fileName, content, Encoding.UTF8);

        public void Add(string filePath)
            => Add(Path.GetFileName(filePath), File.ReadAllBytes(filePath));

        public IEnumerable<string> GetFileList()
            => _itemCache.Keys;
    }
}
