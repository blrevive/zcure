Server framework for Blacklight: Retribution (>3.0).

## documentation

See [Documentation](./Documentation/Readme.md).

## building

- requires .NET8 SDK

### from cli

- `$ dotnet build ZCure.API.csproj -c <Debug/Release>`

### using VS Code

- install [C# extension](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
- open ZCure.API folder with VS Code
- use compile tasks as usual

### using Visual Studio/Rider

- open `ZCure.API.sln` 
- use compile tasks as usual
