using System.Net;
using System.Net.Sockets;
using Serilog;
using ZCure.API.Connections;
using ZCure.API.Requests;

namespace ZCure.API.Servers
{
    public class HttpServer : NetCoreServer.HttpServer, IServer
    {
        public EServerType ServerType => EServerType.Web;
        public ZCureApp App { get; protected set; }
        public Router Router { get; protected set; }

        public string ServerName { get; protected set; }
        public string RegionName { get; protected set; }
        public bool IsRunning { get; protected set; }

        private ILogger _log { get; set; }

        public HttpServer(ZCureApp app, IPAddress ip, int port, string serverName, string regionName)
            : base(ip, port)
        {
            App = app;
            ServerName = serverName;
            RegionName = regionName;
            Router = new Router(app, this);

            _log = Log
                .ForContext<HttpServer>()
                .ForContext("ServerName", serverName);
        }

        protected override NetCoreServer.TcpSession CreateSession()
        {
            return new HttpSession(this);
        }

        protected override void OnConnected(NetCoreServer.TcpSession session)
        {
            _log.Verbose("new connection {Id} from {RemoteEndPoint}", session.Id, session.Socket.RemoteEndPoint.ToString());
            base.OnConnected(session);
        }

        protected override void OnDisconnected(NetCoreServer.TcpSession session)
        {
            _log.Verbose("connection {Id} closed", session.Id);
            base.OnDisconnected(session);
        }

        protected override void OnError(SocketError error)
        {
            _log.Error("Socket Error {SocketError}", error.ToString());
            base.OnError(error);
        }

        public override bool Start()
        {
            Router.BuildRoutingMap();

            IsRunning = base.Start();

            if (IsRunning)
                _log.Information("started http server on {Address}:{Port}", Address, Port);
            else
                _log.Error("failed to start http server on {Address}:{Port}", Address, Port);

            return IsRunning;
        }

        public override bool Stop()
        {
            IsRunning = base.Stop();

            if (!IsRunning)
                _log.Information("stopped http server on {Address}:{Port}", Address, Port);
            else
                _log.Error("failed to stop http server");

            return IsRunning;
        }

        public override bool Restart()
        {
            _log.Information("restarting http server on {Address}:{Port}", Address, Port);
            IsRunning = base.Restart();
            return IsRunning;
        }
    }
}