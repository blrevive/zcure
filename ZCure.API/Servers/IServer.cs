using ZCure.API.Requests;

namespace ZCure.API.Servers
{
    public interface IServer
    {
        public EServerType ServerType { get; }
        public ZCureApp App { get; }
        public Router Router { get; }

        public string ServerName { get; }
        public string RegionName { get; }

        public bool IsRunning { get; }
        public bool IsStarted { get; }

        public abstract bool Start();

        public abstract bool Stop();

        public abstract bool Restart();
    }
}