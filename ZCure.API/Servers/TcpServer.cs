﻿using NetCoreServer;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Requests;

namespace ZCure.API.Servers
{
    public class TcpServer : NetCoreServer.TcpServer, IServer
    {
        public EServerType ServerType => EServerType.Presence;
        public ZCureApp App { get; protected set; }
        public Router Router { get; protected set; }
        public string ServerName { get; protected set; }
        public string RegionName { get; protected set; }
        public bool IsRunning { get; protected set; }

        private ILogger _log;

        public TcpServer(ZCureApp app, IPAddress address, int port, string name = "TCP", string region = "INT")
            : base(address, port)
        {
            _log = Log.ForContext<TcpServer>();
            App = app;
            ServerName = name;
            RegionName = region;
            Router = new Router(app, this);
        }

        protected override NetCoreServer.TcpSession CreateSession()
        {
            return new ZCure.API.Connections.TcpSession(App, this);
        }

        public override bool Start()
        {
            Router.BuildRoutingMap();

            bool started = base.Start();

            if (started)
                _log.Information("started presence server on {Address}:{Port}", Address, Port);
            else
                _log.Error("failed to start presence server on {Address}:{Port}", Address, Port);

            return started;
        }

        public override bool Stop()
        {
            bool stopped = base.Stop();

            if (stopped)
                _log.Information("stopped presence server on {Address}:{Port}", Address, Port);
            else
                _log.Error("failed to stop presence server on {Address}:{Port}", Address, Port);

            return stopped;
        }
    }
}
