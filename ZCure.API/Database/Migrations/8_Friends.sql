CREATE TABLE IF NOT EXISTS `Friends` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `UniqueId` varchar(36) NOT NULL,
  `OwnerId` bigint NOT NULL,
  `FriendId` bigint NOT NULL,
  `State` tinyint(5) NOT NULL,
  `AddedAt` datetime NOT NULL,
  PRIMARY KEY(`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;