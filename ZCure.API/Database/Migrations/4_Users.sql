CREATE TABLE IF NOT EXISTS `Users` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `PassHash` text NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `LastSeenAt` datetime NOT NULL,
  `Experience` int NOT NULL,
  `Gender` tinyint(1) NOT NULL,
  `ProfileFlags` int NOT NULL,
  `Title` int NOT NULL,
  `Badge` int NOT NULL,
  `PrestigeLevel` int NOT NULL,
  `GP` int NOT NULL,
  `ZP` int NOT NULL,
  `Settings` blob,
  `SteamId` bigint NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `Users` (`Id`, `Name`, `PassHash`, `CreatedAt`, `LastSeenAt`, `Experience`, `Gender`, `ProfileFlags`, `Title`, `Badge`, `PrestigeLevel`, `GP`, `ZP`, `SteamId`) VALUES
(0, 'Inventory Template', '', NOW(), NOW(), 0, 0, 0, 0, 0, 0, 0, 0, 0);