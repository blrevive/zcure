﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Serialization;
using Serilog;
using ZCure.API.Connections;
using ZCure.API.Models;
using ZCure.API.Protocol;
using ZCure.API.Serializers;
using NetCoreServer;
using ZCure.API.Servers;

namespace ZCure.API.Requests
{
    public class QueryHandler
    {
        public QueryAttribute Attr { get; private set; }
        public MethodInfo Method { get; private set; }

        public QueryHandler(MethodInfo method)
        {
            Method = method;
            Attr = method.GetCustomAttribute<QueryAttribute>()!;
        }
    }

    /// <summary>
    /// Routes queries to its corresponding query handler.
    /// </summary>
    public class Router
    {
        // map of query handlers and the message type it handles
        private Dictionary<MessageType, QueryHandler> _queryHandlers = new Dictionary<MessageType, QueryHandler>();

        private ILogger _log;

        // delimiter for message data
        private static byte _delimiter = (byte)':';

        private ZCureApp _app;

        private IServer _server;

        public Router(ZCureApp app, IServer server)
        {
            _log = Log.ForContext<Router>();
            _app = app;
            _server = server;
        }

        /// <summary>
        /// Searches for query handlers inside controller classes and adds them to the _queryHandlers map.
        /// </summary>
        public void BuildRoutingMap()
        {
            try {
                _queryHandlers = Assembly.GetExecutingAssembly().GetTypes()
                    .SelectMany(t => t.GetMethods())
                    .Where(m => m.DeclaringType!.IsSubclassOf(typeof(Controller)))
                    .Where(m => {
                        var attr = m.GetCustomAttribute(typeof(QueryAttribute)) as QueryAttribute;
                        if (attr != null && (attr.Server == EServerType.All || attr.Server == _server.ServerType))
                            return true;
                        return false;
                    })
                    .ToDictionary(
                        m => m.GetCustomAttribute<QueryAttribute>()!.MessageType,
                        m => new QueryHandler(m));
                _log.Debug("found {Count} query handlers for {ServerType}", _queryHandlers.Count, _server.ServerType);
            } catch(Exception ex) {
                _log.Fatal(ex, "failed to build request map");
                throw new Exception("failed to build request map", ex);
            }
        }

        public bool NeedsAuthentication(MessageType messageType)
        {
            if (!_queryHandlers.ContainsKey(messageType))
                return false;

            return _queryHandlers[messageType].Attr.Auth;
        }

        /// <summary>
        /// Route and execute a request.
        /// </summary>
        /// <param name="request">request buffer</param>
        /// <returns>serialized response</returns>
        public byte[] Route(Message query, ISession session)
        {
            _log.Debug("routing query (type = {MessageType}; sequence = {Sequence}; server = {ServerType})", query.MessageType, query.SequenceNum, _server.ServerType);

            // create controller 
            var (controller, queryHandler) = GetQueryHandlerInstanceInfo(query);
            controller.Session = session;

            // map message body data to query handler arguments
            object?[] queryHandlerArguments = MapQueryParameters(query, queryHandler.Method);

            // execute query handler
            _log.Debug("executing {QueryHandler} for {MessageType}", $"{controller.GetType().FullName}.{queryHandler.Method.Name}", query.MessageType);
            object result = queryHandler.Method.Invoke(controller, queryHandlerArguments)!;
            
            if (result == null)
                return [];

            // serialize response
            return SerializeResult(result, query);
        }

        /// <summary>
        /// Get request handler method info and controller instance by message type.
        /// </summary>
        /// <param name="messageType">message type</param>
        /// <returns>(controller instance, query handler method)</returns>
        /// <exception cref="Exception">throws exception if no query handler for message type was found</exception>
        private (Controller, QueryHandler) GetQueryHandlerInstanceInfo(Message query)
        {
            // check if query handler for message type exists
            if(!_queryHandlers.ContainsKey(query.MessageType))
                throw new Exception($"no query handler available for message type {query.MessageType} (Data: {Encoding.UTF8.GetString(query.Data)})");
            
            // get method info of query handler
            QueryHandler queryHandler = _queryHandlers[query.MessageType];
            // get controller type
            Type controllerType = queryHandler.Method.DeclaringType!;
            // create instance of controller
            Controller controller = (Controller)Activator.CreateInstance(controllerType)!;

            return (controller, queryHandler);
        }

        /// <summary>
        /// Serialize the result of a query handler to response.
        /// </summary>
        /// <param name="result">data from query handler</param>
        /// <returns>serialized response</returns>
        /// <exception cref="Exception">throws if result is of invalid type</exception>
        private byte[] SerializeResult(object result, Message query)
        {
            Type resultType = result.GetType();

            if (resultType == typeof(void))
                return [];
            if(resultType == typeof(bool))
            {
                Message responseMessage = new Message(query.MessageType, query.SequenceNum, ((bool)result) ? [(byte)'T'] : [(byte)'F']);
                return responseMessage.Encrypt();
            }
            // if result is a message return the encrypted data
            if(resultType == typeof(Message))
            {
                return (result as Message)!.Encrypt();
            }
            // if result is message list return the concatenated encrypted data
            else if (resultType == typeof(List<Message>))
            {
                return (result as List<Message>)!
                    .Select(m => m.Encrypt())
                    .Aggregate((a, b) => a.Concat(b).ToArray());
            } 
            // if result is a value tuple serialize tuple values and return concatenated data
            else if(resultType.FullName!.StartsWith(typeof(ValueTuple).FullName!))
            {
                var dataTuple = result as ITuple;
                List<byte> response = new List<byte>();

                // iterate through values of tuple
                for(int i = 0; i < dataTuple!.Length; i++)
                {
                    var resultData = SerializeResultData(dataTuple[i]!);
                    // add serialized data to response
                    response.AddRange(resultData);

                    // if not last element add the delimiter to response
                    if(i != dataTuple.Length - 1)
                        response.Add((byte)':');
                }

                Message responseMessage = new Message(query.MessageType, query.SequenceNum, response.ToArray());
                return responseMessage.Encrypt();
            }
            // throw exception because result has invalid type
            else 
            {
                throw new Exception($"response data of invalid type {resultType}");
            }
        }

        /// <summary>
        /// Serializes a data fraction of the query handler result.
        /// </summary>
        /// <param name="data">data to serialize</param>
        /// <returns>serialized data</returns>
        private byte[] SerializeResultData(object data)
        {
            switch(data)
            {
                // serialize bool to byte
                case bool b:
                    char c = b ? 'T' : 'F';
                    return [(byte)c];
                // don't serialize byte arrays
                case byte[] bytes:
                    return bytes;
                // serialize enums by its integer represented as string
                case Enum e:
                    return Encoding.UTF8.GetBytes(((int)data).ToString());
                // serialize strings to utf-8 bytes
                case string s:
                    return Encoding.UTF8.GetBytes(s);
                case Guid guid:
                    return Encoding.UTF8.GetBytes(guid.ToString());
                // try to convert to string and return encoded bytes
                default:
                    string str = (string)Convert.ChangeType(data, typeof(string));
                    return Encoding.UTF8.GetBytes(str);
            }
        }

        /// <summary>
        /// Deserialize query data and map it to query handler parameters.
        /// </summary>
        /// <param name="query">query</param>
        /// <param name="queryHandler">query handler</param>
        /// <returns>deserialized parameters</returns>
        /// <exception cref="Exception">deserialization failed</exception>
        private object?[] MapQueryParameters(Message query, MethodInfo queryHandler)
        {
            // get parameter infos of query handler
            ParameterInfo[] queryHandlerParameters = queryHandler.GetParameters();
            // create parameter array
            object?[] parameters = new object[queryHandlerParameters.Length];

            // loop through query handler parameters to deserialize and map the query data 
            int queryDataOffset = 0;
            for(int i = 0; i < queryHandlerParameters.Length; i++)
            {
                ParameterInfo param = queryHandlerParameters[i];

                // get position of the next delimiter
                int nextDelimiter = i == queryHandlerParameters.Length - 1 ? 
                    -1 : Array.IndexOf(query.Data, _delimiter, queryDataOffset);

                // throw exception if no delimiter found but not all parameters are mapped
                if(nextDelimiter == -1 && i != queryHandlerParameters.Length - 1)
                {
                    if(param.IsOptional && param.HasDefaultValue)
                    {
                        parameters[i] = param.DefaultValue;
                        continue;
                    }
                    else
                    { 
                        throw new Exception($"query handler parameter length does not match query data length (expected {queryHandlerParameters.Length} but is {i})");
                    }
                }
                    
                
                // copy bytes from query data to a buffer
                int bufferSize = (nextDelimiter == -1 ? query.Data.Length : nextDelimiter) - queryDataOffset;
                byte[] buffer = new byte[bufferSize];
                Array.Copy(query.Data, queryDataOffset, buffer, 0, bufferSize);

                // deserialize query data to the parameter type
                parameters[i] = DeserializeQueryData(buffer, param);
                queryDataOffset += bufferSize + 1;
            }

            return parameters;
        }

        /// <summary>
        /// Deserializes byte data from query to parameter type
        /// </summary>
        /// <param name="data">query data buffer</param>
        /// <param name="param">query handler parameter info</param>
        /// <returns>deserialized query data</returns>
        /// <exception cref="Exception">deserialization failed</exception>
        private object? DeserializeQueryData(byte[] data, ParameterInfo param)
        {
            // skip deserialization of byte arrays
            if(param.ParameterType == typeof(byte[]))
            {
                return data;
            }
            else
            {
                // get utf-8 string representation of query data
                string stringData = Encoding.UTF8.GetString(data);

                if(stringData == "")
                {
                    if (param.ParameterType.IsValueType)
                        return Activator.CreateInstance(param.ParameterType);
                    else
                        return null;
                }

                // skip deserialization of strings
                if(param.ParameterType == typeof(string))
                {
                    return stringData;
                }
                else if(param.ParameterType == typeof(bool))
                {
                    return stringData == "1" || stringData.ToLower() == "t" || stringData.ToLower() == "true";
                }
                else if(param.ParameterType == typeof(User))
                {
                    long userId = 0;
                    
                    if (!long.TryParse(stringData, out userId))
                        throw new Exception($"invalid user id {stringData}");

                    return _app.Database.Users.Get(userId);
                }
                else if(param.ParameterType == typeof(Guid))
                {
                    return stringData == "" ? Guid.Empty : Guid.Parse(stringData);
                }
                // use ZCXmlSerializer to deserialize parameters annotated with XmlElementAttribute
                else if(param.GetCustomAttribute<XmlElementAttribute>() != null)
                {
                    try {
                        return ZCXmlSerializer.Deserialize(param.ParameterType, stringData);
                    } catch (Exception ex) {
                        throw new Exception($"could not deserialize parameter {param.Name} of type {param.ParameterType} to xml", ex);
                    }
                }
                // use type converter to deserialize other types
                else
                {
                    try {
                        return Convert.ChangeType(stringData, param.ParameterType);
                    } catch(Exception ex) {
                        throw new Exception($"could not convert string value for parameter {param.Name} of type {param.ParameterType}", ex);
                    }
                }
            }
        }
    }
}
