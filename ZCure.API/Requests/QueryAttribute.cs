using ZCure.API.Protocol;
using ZCure.API.Servers;

namespace ZCure.API.Requests
{
    
    [AttributeUsage(AttributeTargets.Method)]
    public class QueryAttribute : Attribute
    {
        public MessageType MessageType { get; private set; }

        public bool Auth { get; private set; }

        public EServerType Server { get; private set; }

        public QueryAttribute(MessageType messageType, bool Auth = true, EServerType Server = EServerType.All)
        {
            MessageType = messageType;
            this.Auth = Auth;
            this.Server = Server;
        }
    }
}