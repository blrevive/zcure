﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API
{
    public class Logger : ILogger
    {
        private ILogger _logger;

        public Logger(ILogger logger)
        {
            _logger = logger;
        }

        /// <inheritdoc />
        public virtual ILogger ForContext(ILogEventEnricher enricher)
        {
            return _logger.ForContext(enricher);
        }

        /// <inheritdoc />
        public virtual ILogger ForContext(IEnumerable<ILogEventEnricher> enrichers)
        {
            return _logger.ForContext(enrichers);
        }

        /// <inheritdoc />
        public virtual ILogger ForContext(string propertyName, object? value, bool destructureObjects = false)
        {
            return _logger.ForContext(propertyName, value, destructureObjects);
        }

        /// <inheritdoc />
        public virtual ILogger ForContext<TSource>()
        {
            return ForContext(typeof(TSource));
        }

        /// <inheritdoc />
        public virtual ILogger ForContext(Type source)
        {
            return _logger
                .ForContext("ClassName", $"[{source.Name}]")
                .ForContext("SourceContext", source.FullName);
        }

        /// <inheritdoc />
        public virtual void Write(LogEvent logEvent)
        {
            _logger.Write(logEvent);
        }

        /// <inheritdoc />
        public virtual void Write(LogEventLevel level, string messageTemplate)
        {
            _logger.Write(level, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Write<T>(LogEventLevel level, string messageTemplate, T propertyValue)
        {
            _logger.Write(level, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Write<T0, T1>(LogEventLevel level, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Write(level, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Write<T0, T1, T2>(LogEventLevel level, string messageTemplate, T0 propertyValue0, T1 propertyValue1,
            T2 propertyValue2)
        {
            _logger.Write(level, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Write(LogEventLevel level, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Write(level, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Write(LogEventLevel level, Exception? exception, string messageTemplate)
        {
            _logger.Write(level, exception, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Write<T>(LogEventLevel level, Exception? exception, string messageTemplate, T propertyValue)
        {
            _logger.Write(level, exception, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Write<T0, T1>(LogEventLevel level, Exception? exception, string messageTemplate, T0 propertyValue0,
            T1 propertyValue1)
        {
            _logger.Write(level, exception, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Write<T0, T1, T2>(LogEventLevel level, Exception? exception, string messageTemplate, T0 propertyValue0,
            T1 propertyValue1, T2 propertyValue2)
        {
            _logger.Write(level, exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Write(LogEventLevel level, Exception? exception, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Write(level, exception, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual bool IsEnabled(LogEventLevel level)
        {
            return _logger.IsEnabled(level);
        }

        /// <inheritdoc />
        public virtual void Verbose(string messageTemplate)
        {
            _logger.Verbose(messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Verbose<T>(string messageTemplate, T propertyValue)
        {
            _logger.Verbose(messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Verbose<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Verbose(messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Verbose<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            _logger.Verbose(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Verbose(string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Verbose(messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Verbose(Exception? exception, string messageTemplate)
        {
            _logger.Verbose(exception, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Verbose<T>(Exception? exception, string messageTemplate, T propertyValue)
        {
            _logger.Verbose(exception, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Verbose<T0, T1>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Verbose(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Verbose<T0, T1, T2>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1,
            T2 propertyValue2)
        {
            _logger.Verbose(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Verbose(Exception? exception, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Verbose(exception, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Debug(string messageTemplate)
        {
            _logger.Debug(messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Debug<T>(string messageTemplate, T propertyValue)
        {
            _logger.Debug(messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Debug<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Debug(messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Debug<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            _logger.Debug(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Debug(string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Debug(messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Debug(Exception? exception, string messageTemplate)
        {
            _logger.Debug(exception, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Debug<T>(Exception? exception, string messageTemplate, T propertyValue)
        {
            _logger.Debug(exception, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Debug<T0, T1>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Debug(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Debug<T0, T1, T2>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1,
            T2 propertyValue2)
        {
            _logger.Debug(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Debug(Exception? exception, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Debug(exception, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Information(string messageTemplate)
        {
            _logger.Information(messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Information<T>(string messageTemplate, T propertyValue)
        {
            _logger.Information(messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Information<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Information(messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Information<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            _logger.Information(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Information(string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Information(messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Information(Exception? exception, string messageTemplate)
        {
            _logger.Information(exception, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Information<T>(Exception? exception, string messageTemplate, T propertyValue)
        {
            _logger.Information(exception, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Information<T0, T1>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Information(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Information<T0, T1, T2>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1,
            T2 propertyValue2)
        {
            _logger.Information(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Information(Exception? exception, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Information(exception, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Warning(string messageTemplate)
        {
            _logger.Warning(messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Warning<T>(string messageTemplate, T propertyValue)
        {
            _logger.Warning(messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Warning<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Warning(messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Warning<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            _logger.Warning(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Warning(string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Warning(messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Warning(Exception? exception, string messageTemplate)
        {
            _logger.Warning(exception, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Warning<T>(Exception? exception, string messageTemplate, T propertyValue)
        {
            _logger.Warning(exception, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Warning<T0, T1>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Warning(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Warning<T0, T1, T2>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1,
            T2 propertyValue2)
        {
            _logger.Warning(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Warning(Exception? exception, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Warning(exception, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Error(string messageTemplate)
        {
            _logger.Error(messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Error<T>(string messageTemplate, T propertyValue)
        {
            _logger.Error(messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Error<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Error(messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Error<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            _logger.Error(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Error(string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Error(messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Error(Exception? exception, string messageTemplate)
        {
            _logger.Error(exception, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Error<T>(Exception? exception, string messageTemplate, T propertyValue)
        {
            _logger.Error(exception, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Error<T0, T1>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Error(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Error<T0, T1, T2>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1,
            T2 propertyValue2)
        {
            _logger.Error(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Error(Exception? exception, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Error(exception, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Fatal(string messageTemplate)
        {
            _logger.Fatal(messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Fatal<T>(string messageTemplate, T propertyValue)
        {
            _logger.Fatal(messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Fatal<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Fatal(messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Fatal<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            _logger.Fatal(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Fatal(string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Fatal(messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual void Fatal(Exception? exception, string messageTemplate)
        {
            _logger.Fatal(exception, messageTemplate);
        }

        /// <inheritdoc />
        public virtual void Fatal<T>(Exception? exception, string messageTemplate, T propertyValue)
        {
            _logger.Fatal(exception, messageTemplate, propertyValue);
        }

        /// <inheritdoc />
        public virtual void Fatal<T0, T1>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            _logger.Fatal(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        /// <inheritdoc />
        public virtual void Fatal<T0, T1, T2>(Exception? exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1,
            T2 propertyValue2)
        {
            _logger.Fatal(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        /// <inheritdoc />
        public virtual void Fatal(Exception? exception, string messageTemplate, params object?[]? propertyValues)
        {
            _logger.Fatal(exception, messageTemplate, propertyValues);
        }

        /// <inheritdoc />
        public virtual bool BindMessageTemplate(string messageTemplate, object?[]? propertyValues, out MessageTemplate parsedTemplate,
            out IEnumerable<LogEventProperty> boundProperties)
        {
            return _logger.BindMessageTemplate(messageTemplate, propertyValues, out parsedTemplate, out boundProperties);
        }

        /// <inheritdoc />
        public virtual bool BindProperty(string? propertyName, object? value, bool destructureObjects, out LogEventProperty property)
        {
            return _logger.BindProperty(propertyName, value, destructureObjects, out property);
        }
    }
}
