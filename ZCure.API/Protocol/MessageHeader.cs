﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Protocol
{
    public class MessageHeader
    {
        public ushort Padding;
        public MessageType MessageType;
        public ushort SequenceNum;

        public byte[] RawHeader { get; protected set; }

        public bool IsValid =>
            RawHeader[0] == 60 && RawHeader[RawHeader.Length - 1] == 62;

        public MessageHeader(byte[] rawHeader)
        {
            RawHeader = rawHeader;
            var vals = Deserialize();
            Padding = vals.Item1;
            MessageType = vals.Item2;
            SequenceNum = vals.Item3;
        }

        public MessageHeader(MessageType messageType, ushort sequenceNum, ushort padding)
        {
            MessageType = messageType;
            SequenceNum = sequenceNum;
            Padding = padding;
            RawHeader = Serialize();
        }

        public byte[] Serialize()
        {
            var header = new byte[8];
            header[0] = 60;
            header[7] = 62;

            BitConverter.GetBytes(Padding).CopyTo(header, 1);
            BitConverter.GetBytes((ushort)MessageType).CopyTo(header, 3);
            BitConverter.GetBytes(SequenceNum).CopyTo(header, 5);

            return header;
        }

        public Tuple<ushort, MessageType, ushort> Deserialize()
        {
            var padding = BytesToUShort(RawHeader[1..3]);
            var messageType = (MessageType)BytesToUShort(RawHeader[3..5]);
            var sequenceNum = BytesToUShort(RawHeader[5..7]);
            return new Tuple<ushort, MessageType, ushort>(padding, messageType, sequenceNum);
        }

        protected static ushort BytesToUShort(byte[] bytes)
            => BitConverter.ToUInt16(!BitConverter.IsLittleEndian ? bytes.Reverse().ToArray() : bytes, 0);
    }
}
