﻿using ProxyProtocol;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Caches;
using ZCure.API.Protocol;
using ZCure.API.Requests;
using ZCure.API.Servers;
using ZCure.API.Services;

namespace ZCure.API.Connections
{
    public class TcpSession : NetCoreServer.TcpSession, ISession
    {
        public ZCureApp App { get; protected set; }

        private ILogger _log;
        private Router _router => (Server as Servers.TcpServer)!.Router;

        private ConnectionCache _connectionCache => App.GetCache<ConnectionCache>()!;
        private AuthCache _authCache => App.GetCache<AuthCache>()!;

        private AuthService _authService => App.GetService<AuthService>()!;

        private bool _isAuthenticated = false;
        public long UserId { get; private set; } = 0;

        private IPAddress RemoteIpAddress = IPAddress.None;

        public TcpSession(ZCureApp app, TcpServer server)
            : base(server)
        {
            App = app;
            _log = Log.ForContext<TcpSession>();
        }

        protected bool AuthSession()
        {
            _log.Debug("new tcp session {Id} from {Endpoint}", Id, Socket.RemoteEndPoint);

            AuthTicket? ticket = _authCache.GetByAddress(RemoteIpAddress);

            if (ticket == null)
            {
                _log.Error("failed to map new tcp session {Id} to cached auth ticket", Id);
                Disconnect();
                return false;
            }

            if(_connectionCache.Contains(ticket.UserId))
            {
                _log.Error("user {UserId} already connected on session {SessionId}", ticket.UserId, _connectionCache.Get(ticket.UserId)!.Id);
                Disconnect();
                return false;
            }

            _connectionCache.Add(ticket.UserId, this);
            _isAuthenticated = true;
            UserId = ticket.UserId;
            _log.Debug("cached tcp session for user {UserId} on {RemoteEndpoint}", ticket.UserId, Socket.RemoteEndPoint!.ToString());
            return true;
        }

        protected override void OnDisconnecting()
        {
            _isAuthenticated = false;
            _connectionCache.Remove(UserId);
            _log.Debug("tcp session {Id} disconnected", Id);
        }

        protected override void OnReceived(byte[] buffer, long offset, long size)
        {
            if(App.Configuration.PresenceServer.ReverseProxy)
            {
                var pp = new ProxyProtocol.ProxyProtocol(new MemoryStream(buffer), (Socket.RemoteEndPoint as IPEndPoint));
                RemoteIpAddress = pp.GetRemoteEndpoint().Result.Address;
                var span = new ReadOnlySpan<byte>(buffer);
                UInt16 headerLen = BitConverter.ToUInt16(span.Slice(14, 2));
                buffer = span.Slice(16 + headerLen).ToArray();
            }
            else
            {
                RemoteIpAddress = (Socket.RemoteEndPoint as IPEndPoint)!.Address;
            }

            if(!_isAuthenticated)
                _isAuthenticated = AuthSession();

            try
            {
                if(!_isAuthenticated)
                {
                    _log.Error("failed to handle request for {Id}: session disconnected", Id);
                    return;
                }

                var messages = ParseMessages(buffer.Take((int)size).ToArray());
                _log.Debug("received {MessageCount} messages", messages.Count);

                foreach(Message query in messages)
                {
                    if (_router.NeedsAuthentication(query.MessageType) && !_authService.ValidateRequest(this, query))
                    {
                        _log.Error("tcp request auth failed for {Id} {MessageType}", Id, query.MessageType);
                        SendAsync(ErrorMessage(query.MessageType).Encrypt());
                        return;
                    }

                    byte[] result = _router.Route(query, this);
                    if (result.Length > 0)
                        SendAsync(result);
                }
            } 
            catch (Exception ex)
            {
                _log.Error(ex, "failed to handle tcp request");
                SendAsync(ErrorMessage(MessageType.Invalid).Encrypt());
            }
        }

        protected List<Message> ParseMessages(byte[] buffer)
        {
            List<Message> messages = new List<Message>();

            int currPos = 0;
            int nextPos = GetNextMessagePosition(buffer, 0);
            
            while(nextPos != -1)
            {
                var msgBuffer = buffer.Skip(currPos).Take(nextPos - currPos).ToArray();
                messages.Add(Message.Decrypt(msgBuffer));

                currPos = nextPos;
                nextPos = GetNextMessagePosition(buffer, currPos);
            }

            var buff = buffer.Skip(currPos).Take(buffer.Length - currPos).ToArray();
            messages.Add(Message.Decrypt(buff));

            return messages;
        }

        protected int GetNextMessagePosition(byte[] buffer, int offset)
        {
            for(int i = offset; i < buffer.Length - 2; i++)
                if (buffer[i] == 62 && buffer[i + 1] == 62
                    && buffer[i + 2] == 60 && buffer[i + 3] == 60)
                    return i + 2;

            return -1;
        }

        protected Message ErrorMessage(MessageType messageType)
            => new Message(messageType, 0, Encoding.UTF8.GetBytes("F"));

        public bool SendMessage(Message message)
            => SendAsync(message.Encrypt());
    }
}
