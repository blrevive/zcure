using Org.BouncyCastle.Asn1.X509.Qualified;
using System.Reflection;

namespace ZCure.API.Configuration
{
    public interface IConfig;

    public class AppConfig : IConfig
    {
        public DatabaseConfig Database { get; set; } = new DatabaseConfig();
        public LoggingConfig Logging { get; set; } = new LoggingConfig();

        public SteamConfig Steam { get; set; } = new SteamConfig();

        public HttpServerConfig WebServer { get; set; } = new HttpServerConfig();
        public PresenceServerConfig PresenceServer { get; set; } = new PresenceServerConfig();

        public string FileCacheDirectory { get; set; } = "./file-cache";

        private static string ToEnvCase(string input)
            => string.Concat(input.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString().ToUpper()));

        public void MapEnvironmentVariables(string prefix = "Zcure", object? owner = null)
        {
            owner = owner ?? this;
            Type type = owner.GetType();;

            var props = type.GetProperties();
            var subconfigs = props.Where(p => p.PropertyType.GetInterface(nameof(IConfig)) != null);
            var opts = props.Where(p => p.PropertyType.GetInterface(nameof(IConfig)) == null);

            foreach (var opt in opts)
                MapEnvironmentVar(ToEnvCase(prefix + opt.Name), opt, owner);

            foreach (var sub in subconfigs)
                MapEnvironmentVariables(prefix + sub.Name, sub.GetValue(owner));
        }

        protected void MapEnvironmentVar(string key, PropertyInfo prop, object owner)
        {
            var value = Environment.GetEnvironmentVariable(key);
            if (value != null)
            {
                object pval;
                Type propType = prop.PropertyType;

                if (propType == typeof(bool))
                    pval = value == "1" || value.ToLower() == "true";
                else if (propType == typeof(string))
                    pval = value;
                else
                    pval = Convert.ChangeType(value, prop.PropertyType);

                prop.SetValue(owner, pval);
            }
        }
    }
}