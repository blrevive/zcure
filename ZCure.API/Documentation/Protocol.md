# message

A message is a structure containing data of a query that should be executed. 

It contains a **header** with metadata which is used for routing and compression and a **body** which contains the actual request/response data.

## structure

- **bom** `byte[2]` *(0x60, 0x60)*: two byte prefix to mark the *begin of message*
- **header**: metadata of the query used for compression and routing
    - **prefix** `byte` *(0x60)*: a prefix byte to verify header
    - **padding** `ushort`: padding added to message body (required due to encryption being chunked in 4 bytes)
    - **type** `ushort`: type of this message (used for routing the query to its handler)
    - **sequence** `ushort`: sequence number used to order multi-sequence queries
    - **postfix** `byte` *(0x62)*: a postfix byte to verify header
- **body**: message body, optionally compressed for long messages
    - (optional) **compression flag** `byte[]` *(0x90, 0x88)*: indicates that the following bytes are compressed using zlib
    - **data** `byte[]`: data of the query serialized to string array delimited by `:` (see [serialization]())
    - (optional) **padding** `byte[0-4]`: padding used to align the message to 4 bytes (required for encryption)
- **eom** `byte[2]` *(0x62, 0x62)*: two byte postfix to mark the *end of message*

## encryption

Messages are encrypted using the [XXTEA](https://en.wikipedia.org/wiki/XXTEA) algorithm which uses 32-bit (4-bytes) chunks (thus the message body is padded to 4 bytes). 

The encryption of a message happens in two steps:

- message body is encrypted
- message header + body are encrypted together

> The bom and eom of a message are not subject to encryption.

The implementation and keys of the XXTEA implementation can be seen inside the [ZCure.API/Crypt/XXTEA.cs](../Crypt/XXTEA.cs) class (untouched and retrieved from the decompiled original ZCApi).

## serialization

To prevent data alignment/encoding errors the message body data contains a UTF-8 string array (delimited by `:`) which represents the actual data. All data transfered thus is first serialized to its according string representation and concatenated using `:` as delimiter. There are some limitations to this approach which requires attention while serialization:

- pure binary data (for eg the user settings are stored and transfered using a binary blob) can not be properly serialized as string
    - binary data **must** be the last element of the data array
    - binary data is not serialized and simply appended to the message body data bytes instead
- some string data (for eg the store data) *can* contain the `:` char even tho it is not meant to delimit the message body
    - this data **must** always be the last element of the data array
    - while serialization this can be ignored
    - while deserialization it requires to know the element count of the data array (so the string is not accidently split)

# request

Request refers to the raw body recieved from the client (http body or udp data). A request contains at least one encrypted message (but can contain more).

- a http request contains a `UserID` header with the id of the user sending the request
    - for requests sent before authentication succeeded this is always 0

# response

Response refers to the raw body which is send back to the client (http body or udp data). Similar to the request a response contains at least one encrypted message.

- the first element of the message body data string array of a response is a `char` indicating wether the query executed succesfully
    - `T`: query executed succesfully
    - `F`: query failed to execute