The ZCure.API library provides all functionalities required to assemble a main server for post-parity Blacklight: Retribution (>3.0). It is a complete rewrite of the original [`ZCAPI`]() found inside the `BlrCrashReporter.exe` of older BL:R clients and mimics the original communication patterns whilst using modern design patterns and frameworks. 

## frameworks used

- `EntityFramework.Core` as ORM provider
    - this decision basicly is justified by the ease of use and popularity (dapper was considered as alternative and may be targeted in future to improve performance)
- `NetCoreServer` as networking backend
    - strong framework which provides easy to configure and high-performant servers for several different network protocols

## compliance

This library was designed to comply with the original communication workflow which is documented in [Documentation/Servers](./Servers.md).

Also it provides a refactored version of the ZCure message protocol which is documented in [Documentation/Protocol](./Protocol.md).

## routing

The way requests/messages are routed was redesigned completely to stick to modern standards and improve maintability/readability of the code.

An overview of the routing workflow is available at [Documentation/Routing](./Routing.md).

[`ZCApi`]: https://gitlab.com/blrevive/reverse/blrcrashreporter/-/tree/1.12-Arc/ZCAPI?ref_type=heads