using System.Security.Principal;
using ZCure.API.Protocol;
using ZCure.API.Models;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class InventoryController : Controller
    {
        [Query(MessageType.GetInventory)]
        public (bool, long, string) QueryInventory(User? user, string InvalidateCached)
        {
            if (user == null)
            {
                _log.Error($"failed to get inventory: no such user");
                return (false, 0, "");
            }

            // @todo find a performant way to create unlocked inventory item list for specific user
            var inventoryItems = _database.Inventory.GetList().ToList();
            foreach(var item in inventoryItems)
                item.Owner = user;
            
            var inventory = new Inventory(user, inventoryItems);
            var inventoryString = ZCXmlSerializer.Serialize(inventory);
            return (true, user.Id, inventoryString);
        }

        [Query(MessageType.CheckInventory)]
        public bool CheckInventory(long userId, string lastTime)
        {
            return true;
        }

        [Query(MessageType.ActivateInventoryItem)]
        public (bool, EPurchaseResult) ActivateInventoryItem(User user, Guid itemId, string data)
        {
            var inventoryItem = _database.Inventory.Get(itemId);

            if(inventoryItem != null && inventoryItem.UnlockId == 74)
            {
                ClanController clanController = new() { Session = this.Session };
                clanController.CreateClan(user, data);
                return (true, EPurchaseResult.Success);
            }

            return (false, EPurchaseResult.UnknownError);
        }
    }
}