﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models.Game;
using ZCure.API.Requests;
using ZCure.API.Services;

namespace ZCure.API.Controllers
{
    public class MatchmakingController : Controller
    {
        private GameServerService _gameServerService => _app.GetService<GameServerService>()!;

        [Query(Protocol.MessageType.GetServers)]
        public (bool, int, string) GetServerList(string filters, int queryId)
        {
            var infos = _gameServerService.GetGameServerInfos();
            var str = String.Join(":", infos.Select(i => i.GetAdvertisedString()));

            return (true, queryId, str);
        }

        [Query(Protocol.MessageType.CreatePrivateMatch)]
        public bool CreatePrivateMatch(string opts)
        {
            return true;
        }
    }
}
