using System.Text;
using ZCure.API.Protocol;
using ZCure.API.Models;
using ZCure.API.Requests;
using ZCure.API.Serializers;
using ZCure.API.Models.Store;
using System.Net.Sockets;
using ZCure.API.Caches;
using ZCure.API.Services;

namespace ZCure.API.Controllers
{

    public enum EPurchaseResult
    {
        Success,
        InsufficientFunds,
        InvalidItem,
        InvalidPrice,
        MaxOwned,
        InventoryCapped,
        NeedsItem,
        NoLongerAvailable,
        QuantityNotAvailable,
        InvalidRecipient,
        RequiresGPPermLicense,
        UnknownError,
    }

    public class StoreController : Controller
    {
        private StoreCache _storeCache => _app.GetCache<StoreCache>()!;
        private PresenceService _presenceService => _app.GetService<PresenceService>()!;

        [Query(MessageType.GetUserstore)]
        public List<Message> GetUserStore(long userId)
        {
            return new List<Message>()
            {
                new Message(MessageType.GetOffers, 0, Encoding.UTF8.GetBytes(_storeCache.GetCachedOffers())),
                new Message(MessageType.GetUserstore, 0, Encoding.UTF8.GetBytes(_storeCache.GetCachedStore()))
            };
        }

        [Query(MessageType.PurchaseItem)]
        public (bool, EPurchaseResult, Guid) PurchaseItem(
            User? user, Guid itemId, int amount, int purchaseLength, int location, int currency, int purchaseType, 
            string data = "", int associatedUnlockId = -1)
        {
            if (user == null)
                return (false, EPurchaseResult.UnknownError, Guid.Empty);

            StoreItem? storeItem = _database.Store.Get(itemId);
            if (storeItem == null)
                return (false, EPurchaseResult.InvalidItem, itemId);

            var inventoryItem = InventoryItem.FromStoreItem(storeItem, user);
            inventoryItem.AssociatedUnlockId = associatedUnlockId;
            inventoryItem.PurchaseDuration = purchaseLength;
            inventoryItem.PurchaseType = (EPurchaseType)purchaseType;
            inventoryItem.Quantity = amount;
            inventoryItem.UpdateTypeKey = "gen";

            _database.Inventory.Insert(inventoryItem);

            Inventory updateInventory = new Inventory();
            updateInventory.Owner = user;
            updateInventory.InventoryItems = new List<InventoryItem>() { inventoryItem };

            _presenceService.SendNotification(user, MessageType.InventoryUpdated, $"ZCInventory:{user.Id}:{ZCXmlSerializer.Serialize(updateInventory)}");

            return (true, EPurchaseResult.Success, inventoryItem.InstanceId);
        }
    }
}