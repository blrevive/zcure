using ZCure.API.Models;
using ZCure.API.Protocol;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class StatsController : Controller
    {
        [Query(MessageType.GetUserStats)]
        public (bool, string) GetUserStats(long userId)
        {
            var userStats = _database.Stats.Get(userId);

            if (userStats == null)
            {
                var user = _database.Users.Get(userId);

                if (user == null)
                {
                    _log.Error($"failed to create default stats: no such user {userId}");
                    return (false, "");
                }

                userStats = new UserStats()
                {
                    UserId = user.Id,
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    Stats = new List<UserStat>()
                };

                _database.Stats.Insert(userStats);
            }

            string stats = ZCXmlSerializer.Serialize(userStats);
            return (true, stats);
        }
    }
}