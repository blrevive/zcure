using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [XmlRoot("ZCInventory")]
    public class Inventory
    {
        [XmlIgnore]
        private const int DefaultMaxItems = 1000;

        [XmlIgnore]
        public User Owner { get; set; }
        
        [XmlAttribute("UID")]
        [NotMapped]
        public string OwnerId { get { return Owner.Id.ToString("D19"); } set{} }
        
        [XmlAttribute("IC")]
        public int MaxItems = DefaultMaxItems;

        [XmlIgnore]
        public bool CapReached => this.SlotsLeft <= 0;

        [XmlIgnore]
        public int SlotsLeft => this.MaxItems - this.InventoryItems.Count;

        [XmlElement("II")]
        public List<InventoryItem> InventoryItems { get; set; }

        [XmlElement("PM")]
        public List<PermanentItem> PermanentItems  { get; set; }

        public Inventory(User owner, List<InventoryItem> items)
        {
            Owner = owner;
            InventoryItems = items.Where(i => !i.IsPermanent).ToList();
            PermanentItems = items.Where(i => i.IsPermanent).Select(i => i.ToPermanentItem()).ToList();
        }

        public Inventory()
        {
            Owner = null;
            InventoryItems = new List<InventoryItem>();
            PermanentItems = new List<PermanentItem>();
        }
    }
}