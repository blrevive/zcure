using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [Serializable]
    public enum EFriendshipState
    {
        [XmlEnum("0")]
        Pending,
        [XmlEnum("1")]
        Accepted,
        [XmlEnum("2")]
        Denied
    }

    [Table("Friends")]
    [XmlRoot("ZCFriendItem")]
    public class ZCFriendItem
    {
        [Key]
        [XmlIgnore]
        public long Id { get; set; }

        [XmlAttribute("UID")]
        public Guid UniqueId {get; set;}

        [XmlAttribute("OW")]
        public long OwnerId {get; set;}

        [XmlAttribute("FID")]
        public long FriendId {get; set;}

        [XmlAttribute("AT")]
        public DateTime AddedAt { get; set; }

        [XmlAttribute("FS")]
        public EFriendshipState State {get; set;}
    }
}