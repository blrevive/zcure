﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [XmlRoot("ZCMailItem")]
    public class ZCMailItem
    {
        [Key]
        [XmlAttribute("UID")]
        public Guid UniqueId {get; set;}

        [XmlIgnore]
        public User Sender {get; set; }

        [XmlAttribute("SID")]
        public long SenderId {get; set;}

        [NotMapped]
        [XmlAttribute("SN")]
        public string SenderName { get { return Sender.Name;} set {}}

        [XmlAttribute("TS")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [XmlAttribute("XD")]
        public DateTime ExpireAt { get; set; } = DateTime.Now.AddYears(1);

        [XmlAttribute("RD")]
        [DefaultValue(false)]
        public bool Read { get; set; } = false;

        [XmlAttribute("SB")]
        [DefaultValue("")]
        public string Subject { get; set; } = "";

        [XmlAttribute("MS")]
        [DefaultValue("")]
        public string Message { get; set; } = "";

        [XmlAttribute("MD")]
        [DefaultValue("<Root/>")]
        public string MailData {get; set;} = "<Root/>";

        [XmlAttribute("GP")]
        [DefaultValue(0)]
        public int GP {get; set;} = 0;
    }
}
