using Dapper;
using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    [Table("Clans")]
    public class ZCClan
    {
        [Key]
        [XmlAttribute("CID")]
        public Guid Id { get; set; }

        [XmlAttribute("CN")]
        public string Name { get; set; }
        
        [XmlAttribute("CT")]
        public string Tag {get; set;}

        [XmlAttribute("OID")]
        public long OwnerId { get; set; }

        [XmlAttribute("MOTD")]
        public string MOTD { get; set; }

        [XmlAttribute("DF")]
        public DateTime FoundedAt { get; set; }

        [XmlAttribute("ML")]
        public ushort MemberLimit { get; set; }
    }
}