﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models.Presence
{
    public enum EOnlineContext
    {
        Offline,
        NotInGame,
        Away,
        Busy,
        Online,
        Customizing,
        Shopping,
        PreGame,
        Playing,
        EndRound,
        Intermission,
        RankedMatch,
        PracticeMatch
    }

    public class PresenceInfo
    {
        [XmlIgnore]
        public User User { get; set; }

        [XmlAttribute("SC")]
        public EOnlineContext StatusContext { get; set; }

        [XmlAttribute("PL")]
        public string Location { get; set; }

        [XmlAttribute("UID")]
        public long UserId { get; set; }

        public override string ToString()
        {
            return $"User={UserId};StatusContext={StatusContext};Location={Location}";
        }
    }
}
