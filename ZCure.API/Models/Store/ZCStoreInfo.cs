﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models.Store
{
    public class ZCStoreTag
    {
        [XmlAttribute("TN")]
        public string Tag;

        [XmlElement("ST")]
        public List<ZCStoreTag> ChildTags;
    }

    public class ZCStoreInfo
    {
        [XmlAttribute("RT")]
        public DateTime NextReadTime { get; set; }

        [XmlElement("ST")]
        public ZCStoreTag[] StoreTags { get; set; }

        [XmlElement("SI")]
        public StoreItem[] StoreItems { get; set; }
    }
}
