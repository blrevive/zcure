using System.Xml.Serialization;

namespace ZCure.API.Models.Store
{
    [XmlRoot("ZCStoreInfo")]
    public class StoreInfo
    {
        [XmlAttribute("RT")]
        public DateTime NextReadTime { get; set; }

        [XmlElement("ST")]
        public List<StoreTag> Tags { get; set; } = new List<StoreTag>();

        [XmlElement("SI")]
        public List<StoreItem> Items { get; set; } = new List<StoreItem>();
    }
}