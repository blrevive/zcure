﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ZCure.API.Serializers;

namespace ZCure.API.Models.Store
{
    public enum EActivationType : int
    {
        [XmlEnum("1")]
        Rental,
        [XmlEnum("2")]
        Apply,
        [XmlEnum("3")]
        InputString,
        [XmlEnum("4")]
        Consume,
        [XmlEnum("5")]
        Stackable,
        [XmlEnum("6")]
        None
    }

    [Table("Store")]
    [XmlRoot("StoreItem", Namespace = "")]
    public class StoreItem
    {
        [XmlAttribute("IG")]
        public Guid Id { get; set; }

        [XmlAttribute("ID")]
        public int ItemId { get; set; }

        [XmlAttribute("CD")]
        public DateTime CreatedAt { get; set; }

        [XmlAttribute("MD")]
        public DateTime ModifiedAt { get; set; }

        [XmlAttribute("POD")]
        public DateTime PreOrderAt { get; set; }

        [XmlAttribute("LD")]
        public DateTime LiveAt { get; set; }

        [XmlAttribute("XD")]
        public DateTime ExpireAt { get; set; }

        [XmlAttribute("AT")]
        [DefaultValue(EActivationType.Rental)]
        public EActivationType ActivationType { get; set; }

        [XmlAttribute("AD")]
        [DefaultValue("<Root />")]
        public string ActivationData { get; set; } = "<Root/>";

        [NotMapped]
        [XmlElement("CAD")]
        [DefaultValue("<Root />")]
        public string ClientActivationData
        {
            get => !SendActivationData ? "<Root />" : ActivationData
                .Replace("ArrayOfBaseItem", "ITEMS")
                .Replace("ArrayOfRandomItem", "ITEMS")
                .Replace("Duration", "DUR")
                .Replace("Quantity", "QTY")
                .Replace("Weight", "WGT")
                .Replace("BaseItem", "ITEM")
                .Replace("RandomItem", "ITEM")
                .Replace("Items", "ITEMS");
            set => ActivationData = value;
        }

        [XmlAttribute("SAD")]
        [DefaultValue(false)]
        public bool SendActivationData { get; set; }

        [XmlAttribute("LV")]
        public int UnlockLevel { get; set; }

        [XmlAttribute("VL")]
        [DefaultValue(0)]
        public short Visibility { get; set; }

        [XmlAttribute("HD")]
        [DefaultValue(false)]
        public bool Hidden { get; set; }

        [XmlElement("PR")]
        [NotMapped]
        public List<StoreItemPrice> Prices
        {
            get => ZCXmlSerializer.Deserialize<List<StoreItemPrice>>(PriceData);
            set => PriceData = ZCXmlSerializer.Serialize(value);
        }

        public string PriceData { get; set; }

        [XmlElement("MD")]
        [NotMapped]
        public StoreItemMetaData MetaData
        {
            get
            {
                return new StoreItemMetaData()
                {
                    FriendlyName = FriendlyName,
                    ItemTags = Tags,
                    Rarity = Rarity
                };
            }
            set { }
        }

        [XmlIgnore]
        public string FriendlyName { get; set; } = "";

        [XmlIgnore]
        public string Tags { get; set; } = "";

        [XmlIgnore]
        public int Rarity { get; set; }
    }
}
