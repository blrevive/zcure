﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models.Store
{
    [Table("Offers")]
    [XmlRoot("ZCStoreOfferItem")]
    public class StoreOfferItem
    {
        [Key]
        [XmlAttribute("OG")]
        public Guid OfferId { get; set; } = Guid.NewGuid();

        [XmlAttribute("IG")]
        public Guid ItemId { get; set; } = Guid.Empty;

        [XmlAttribute("TN")]
        [DefaultValue("")]
        public string Tag { get; set; } = "";

        [XmlAttribute("LD")]
        public DateTime StartAt { get; set; } = DateTime.Now;

        [XmlAttribute("XD")]
        public DateTime EndAt { get; set; } = DateTime.Now.AddMonths(2);

        [XmlAttribute("OD")]
        [DefaultValue("")]
        public string OfferData { get; set; } = "";
    }
}
