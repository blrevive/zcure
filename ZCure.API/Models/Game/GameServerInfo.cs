﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Models.Game
{
    public enum EGameMap
    {
        Deadlock,
        Piledriver,
        HeavyMetal,
        Vertigo,
        Containment,
        Vortex,
        HeloDeck,
        SeaPort,
        Decay,
        Evac,
        Trench,
        Metro,
        Centre,
        Shelter,
        Safehold,
        Lockdown,
        DeathMetal,
        Terminus,
        Outpost,
        Convoy,
        Rig,
        Crashsite,
        GunRange,

        FoxEntry = -1
    }

    public enum EGameMode
    {
        DM,
        TDM,
        DOM,
        KOTH,
        CTF,
        CTDM,
        Netwar,
        Siege,
        KC,
        OS,
        SND,
        LTS,
        LMS,
        Train
    }


    public class GameServerInfo : ICloneable
    {
        public long ServerId { get; set; }
        public string ServerName { get; set; }
        public string Address { get; set; }
        public int Port { get; set; }
        public string Password { get; set; } = "";

        public int CurrentPlayerCount { get; set; } = 0;
        public int MaxPlayerCount { get; set; } = 0;

        public EGameMap CurrentMap { get; set; }
        public EGameMode GameMode { get; set; }

        public string OwnerName { get; set; } = "";

        public int MinLevel { get; set; } = 0;
        public int MaxLevel { get; set; } = 0;

        public int GoalScore { get; set; } = 100;

        public string GetAdvertisedString()
        {
            string properties = $"MapIndex={(int)CurrentMap},GameIndex={(int)GameMode},GoalScore={GoalScore},GameVersion=302,ServerName={ServerName},ServerID={ServerId},OwnerName={OwnerName},PlaylistName={ServerName}";
            string baseInfo = $"{ServerId}:{Address}:{Port}:{CurrentPlayerCount}:{MaxPlayerCount}:{(Password == "" ? "false" : "true")}:{OwnerName}";

            return $"{baseInfo}:{properties}";
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
